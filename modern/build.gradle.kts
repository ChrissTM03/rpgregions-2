val ultraRegionsSupport = (properties.getValue("ultraRegionsSupport") as String).toBoolean()

tasks.compileJava {
    if (!ultraRegionsSupport) {
        sourceSets.main.get().java.exclude("**/net/islandearth/rpgregions/api/integrations/ultraregions/UltraRegionsIntegration.java")
    }
}

dependencies {
    testImplementation("junit:junit:4.13.2")

    compileOnly("org.spigotmc:spigot-api:1.18.2-R0.1-SNAPSHOT")
    compileOnly("com.sk89q.worldguard:worldguard-bukkit:7.0.4-SNAPSHOT") {
        exclude("com.destroystokyo.paper")
        exclude("org.spigotmc")
    }
    compileOnly(":Residence4.9.2.2") // residence
    compileOnly(":GriefPrevention") // griefprevention
    if (ultraRegionsSupport) compileOnly(":UltraRegions") // ultraregions

    compileOnly("org.jetbrains:annotations:23.0.0")
    compileOnly(project(":api"))
    compileOnly(project(":rpgregions"))
}