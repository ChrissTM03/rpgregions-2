package net.islandearth.rpgregions.managers.data.account;

import net.islandearth.rpgregions.managers.data.region.Discovery;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class RPGRegionsAccount {

    private final UUID uuid;
    private final Map<String, Discovery> discoveredRegions;
    private final List<AccountCooldown> cooldowns;

    public RPGRegionsAccount(UUID uuid, Map<String, Discovery> discoveredRegions) {
        this.uuid = uuid;
        this.discoveredRegions = discoveredRegions;
        this.cooldowns = new ArrayList<>();
    }

    public UUID getUuid() {
        return uuid;
    }

    public Map<String, Discovery> getDiscoveredRegions() {
        return discoveredRegions;
    }

    public void addDiscovery(Discovery discovery) {
        discoveredRegions.put(discovery.getRegion(), discovery);
    }

    public List<AccountCooldown> getCooldowns() {
        return cooldowns;
    }

    public enum AccountCooldown {
        ICON_COMMAND,
        TELEPORT
    }
}