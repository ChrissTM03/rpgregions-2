package net.islandearth.rpgregions.managers.data;

import com.google.common.collect.ImmutableMap;
import net.islandearth.rpgregions.managers.data.region.ConfiguredRegion;

import java.util.Optional;
import java.util.concurrent.CompletableFuture;

public interface IRPGRegionsCache {

    Optional<ConfiguredRegion> getConfiguredRegion(String id);

    void addConfiguredRegion(ConfiguredRegion region);

    void removeConfiguredRegion(String id);

    ImmutableMap<String, ConfiguredRegion> getConfiguredRegions();

    void clear();

    CompletableFuture<Boolean> saveAll(boolean async);
}
