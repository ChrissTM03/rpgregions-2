package net.islandearth.rpgregions.managers.data.region;

import com.google.gson.Gson;
import me.arcaniax.hdb.api.HeadDatabaseAPI;
import net.islandearth.rpgregions.api.IRPGRegionsAPI;
import net.islandearth.rpgregions.api.RPGRegionsAPI;
import net.islandearth.rpgregions.command.IconCommand;
import net.islandearth.rpgregions.editor.annotate.EditableField;
import net.islandearth.rpgregions.editor.annotate.NeedsGUI;
import net.islandearth.rpgregions.effects.RegionEffect;
import net.islandearth.rpgregions.regenerate.Regenerate;
import net.islandearth.rpgregions.requirements.RegionRequirement;
import net.islandearth.rpgregions.rewards.DiscoveryReward;
import net.islandearth.rpgregions.translation.Translations;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ConfiguredRegion {

    private final UUID world;
    private final String id;
    private String customName;
    private final List<DiscoveryReward> rewards;
    @NeedsGUI private Sound sound;
    @NeedsGUI private String icon;
    @NeedsGUI private String undiscoveredIcon;
    private final List<IconCommand> iconCommand;
    @EditableField(description = "Toggle whether the coordinates of the region are shown", name = "Toggle coordinates")
    @NeedsGUI private final boolean showCoords;
    @EditableField(material = Material.ENDER_PEARL, name = "Set teleport location", description = "Set the teleport location to your current location")
    @NeedsGUI private Location location;
    @NeedsGUI private final List<String> hints;
    @EditableField(description = "Toggle whether the hint is shown", name = "Toggle hint")
    @NeedsGUI private final boolean showHint;
    private boolean teleportable;
    private boolean hidden;
    private boolean discoverable;
    private final List<RegionEffect> effects;
    private final List<RegionRequirement> requirements;
    private final List<String> discoveredLore;
    @EditableField(description = "Toggle whether the title is always shown on entry after discovery", name = "Toggle always showing titles")
    @NeedsGUI private final boolean alwaysShowTitles;
    private List<String> title;
    private List<String> subtitle;
    private List<String> discoveredTitle;
    private List<String> discoveredSubtitle;
    private Regenerate regenerate;
    @EditableField(material = Material.NETHER_STAR, name = "Set teleport cooldown", description = "Set the cooldown for teleportation")
    private int teleportCooldown;
    @NeedsGUI private boolean showActionbar;
    @EditableField(material = Material.RED_DYE, name = "Hex display colour", description = "Set the colour of the region. It is a hex colour (e.g 0x42f4f1 for red) and is used in dynmap.")
    private final String colour;
    private final String lineColour;
    @NeedsGUI private final boolean dynmap;

    public ConfiguredRegion(@Nullable World world, String id, String customName,
                            List<DiscoveryReward> rewards, List<RegionEffect> effects) {
        this.world = world != null ? world.getUID() : null;
        this.id = id;
        this.customName = customName;
        this.rewards = rewards;
        this.sound = Sound.UI_TOAST_CHALLENGE_COMPLETE;
        Optional<Material> defaultIcon = Optional.of(Material.valueOf(RPGRegionsAPI.getAPI().getConfig().getString("settings.server.gui.default_region_icon")));
        this.icon = defaultIcon.map(Enum::name).orElseGet(Material.TOTEM_OF_UNDYING::name);
        this.undiscoveredIcon = defaultIcon.map(Enum::name).orElseGet(Material.TOTEM_OF_UNDYING::name);
        this.iconCommand = new ArrayList<>();
        this.showCoords = false;
        this.hints = new ArrayList<>();
        this.showHint = false;
        this.teleportable = false;
        this.hidden = false;
        this.discoverable = true;
        this.effects = effects;
        this.requirements = new ArrayList<>();
        this.discoveredLore = new ArrayList<>();
        this.alwaysShowTitles = false;
        this.teleportCooldown = 0;
        this.showActionbar = true;
        this.colour = String.valueOf(13369344);
        this.lineColour = String.valueOf(13369344);
        this.dynmap = true;
    }

    public ConfiguredRegion(@Nullable World world, String id, String customName,
                            List<DiscoveryReward> rewards, List<RegionEffect> effects, Sound sound, Material icon) {
        this(world, id, customName, rewards, effects);
        this.sound = sound;
        this.icon = icon.name();
    }

    public String getId() {
        return id;
    }

    public String getCustomName() {
        return customName;
    }

    public void setCustomName(String customName) {
        this.customName = customName;
    }

    @Nullable
    public List<DiscoveryReward> getRewards() {
        return rewards;
    }

    @Nullable
    public Sound getSound() {
        return sound;
    }

    @Nullable
    public ItemStack getIcon() {
        if (icon == null) return new ItemStack(Material.TOTEM_OF_UNDYING);
        if (icon.startsWith("hdb-") && RPGRegionsAPI.getAPI().hasHeadDatabase()) return new ItemStack(new HeadDatabaseAPI().getItemHead(icon.replace("hdb-", "")));
        return new ItemStack(Material.valueOf(icon));
    }

    public void setIcon(@NotNull Material material) {
        this.icon = material.name();
    }

    @Nullable
    public ItemStack getUndiscoveredIcon() {
        if (undiscoveredIcon == null) return new ItemStack(Material.TOTEM_OF_UNDYING);
        if (undiscoveredIcon.startsWith("hdb-") && RPGRegionsAPI.getAPI().hasHeadDatabase()) return new ItemStack(new HeadDatabaseAPI().getItemHead(undiscoveredIcon.replace("hdb-", "")));
        return new ItemStack(Material.valueOf(undiscoveredIcon));
    }

    public void setUndiscoveredIcon(@NotNull Material material) {
        this.undiscoveredIcon = material.name();
    }

    @NotNull
    public List<IconCommand> getIconCommand() {
        return iconCommand == null ? new ArrayList<>() : iconCommand;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public boolean isShowCoords() {
        return showCoords;
    }

    @Nullable
    public List<String> getHints() {
        return hints;
    }

    public boolean isShowHint() {
        return showHint;
    }

    public boolean isTeleportable() {
        return teleportable;
    }

    public void setTeleportable(boolean teleportable) {
        this.teleportable = teleportable;
    }

    @Nullable
    public World getWorld() {
        return Bukkit.getWorld(world);
    }

    public boolean isDiscoverable() {
        return discoverable;
    }

    public void setDiscoverable(boolean discoverable) {
        this.discoverable = discoverable;
    }

    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    @Nullable
    public List<RegionEffect> getEffects() {
        return effects;
    }

    @Nullable
    public List<RegionRequirement> getRequirements() {
        return requirements;
    }

    @Nullable
    public List<String> getDiscoveredLore() {
        return discoveredLore;
    }

    public boolean alwaysShowTitles() {
        return alwaysShowTitles;
    }

    /**
     * Gets the region title for a player. If region title is null, the translation files will be used.
     * @param player the player
     * @return A string list of title
     */
    @NotNull
    public List<String> getTitle(Player player) {
        if (title == null) {
            return Translations.DISCOVERED_TITLE.get(player, customName);
        }

        List<String> translatedTitle = new ArrayList<>();
        title.forEach(titles -> translatedTitle.add(ChatColor.translateAlternateColorCodes('&', titles)));
        return translatedTitle;
    }

    /**
     * Gets the region subtitle for a player. If region subtitle is null, the translation files will be used.
     * @param player the player
     * @return A string list of subtitles
     */
    @NotNull
    public List<String> getSubtitle(Player player) {
        if (subtitle == null) {
            return Translations.DISCOVERED_SUBTITLE.get(player, customName);
        }

        List<String> translatedSubtitle = new ArrayList<>();
        subtitle.forEach(sub -> translatedSubtitle.add(ChatColor.translateAlternateColorCodes('&', sub)));
        return translatedSubtitle;
    }

    /**
     * Gets the region discovered title for a player. If region title is null, the translation files will be used.
     * @param player the player
     * @return A string list of title
     */
    @NotNull
    public List<String> getDiscoveredTitle(Player player) {
        if (discoveredTitle == null) {
            return Translations.ALREADY_DISCOVERED_TITLE.get(player, customName);
        }

        List<String> translatedTitle = new ArrayList<>();
        discoveredTitle.forEach(titles -> translatedTitle.add(ChatColor.translateAlternateColorCodes('&', titles)));
        return translatedTitle;
    }

    /**
     * Gets the region discovered subtitle for a player. If region subtitle is null, the translation files will be used.
     * @param player the player
     * @return A string list of subtitles
     */
    @NotNull
    public List<String> getDiscoveredSubtitle(Player player) {
        if (discoveredSubtitle == null) {
            return Translations.ALREADY_DISCOVERED_SUBTITLE.get(player, customName);
        }

        List<String> translatedSubtitle = new ArrayList<>();
        discoveredSubtitle.forEach(sub -> translatedSubtitle.add(ChatColor.translateAlternateColorCodes('&', sub)));
        return translatedSubtitle;
    }

    @Nullable
    public Regenerate getRegenerate() {
        return regenerate;
    }

    public void setRegenerate(@NotNull Regenerate regenerate) {
        this.regenerate = regenerate;
    }

    public int getTeleportCooldown() {
        return teleportCooldown;
    }

    public void setTeleportCooldown(int teleportCooldown) {
        this.teleportCooldown = teleportCooldown;
    }

    public boolean showActionbar() {
        return showActionbar;
    }

    public void setShowActionbar(boolean showActionbar) {
        this.showActionbar = showActionbar;
    }

    @Nullable
    public List<Location> getBoundingBox() {
        return RPGRegionsAPI.getAPI().getManagers().getIntegrationManager().getBoundingBoxPoints(location, id);
    }

    public String getColour() {
        if (colour == null) return "0";
        return colour;
    }

    public String getLineColour() {
        if (lineColour == null) return "0";
        return lineColour;
    }

    public boolean isDynmap() {
        return dynmap;
    }

    public File getFile(IRPGRegionsAPI plugin) {
        File file = null;
        try {
            file = this.findFile(plugin);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (file == null) file = new File(plugin.getDataFolder() + "/regions/" + this.id + ".json");
        return file;
    }

    public void save(IRPGRegionsAPI plugin) {
        try {
            File file = this.findFile(plugin);
            if (file == null) file = new File(plugin.getDataFolder() + "/regions/" + this.id + ".json");
            try (Writer writer = new FileWriter(file)) {
                Gson gson = plugin.getGson();
                gson.toJson(this, writer);
                writer.flush();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean delete(IRPGRegionsAPI plugin) {
        try {
            File file = this.findFile(plugin);
            if (file == null) file = new File(plugin.getDataFolder() + "/regions/" + this.id + ".json");
            return Files.deleteIfExists(file.toPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Nullable
    private File findFile(IRPGRegionsAPI plugin) throws IOException {
        File folder = new File(plugin.getDataFolder() + "/regions/");
        Stream<Path> files = Files.walk(Paths.get(folder.getPath()));
        List<Path> valid = files.filter(path -> Files.isRegularFile(path) && path.toFile().getName().equals(this.id + ".json"))
                .collect(Collectors.toList());
        files.close();
        if (valid.isEmpty()) return null;
        if (valid.size() > 1) {
            plugin.getLogger().severe("Duplicate region files have been found for " + this.id + ". " +
                    "In order to protect your data, the plugin will NOT save the region config.");
            throw new IOException("Duplicate region file");
        }

        File file = null;
        for (Path path : valid) {
            file = path.toFile();
        }
        return file;
    }
}
