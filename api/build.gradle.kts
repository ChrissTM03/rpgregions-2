plugins {
    id("maven-publish")
}

dependencies {
    testImplementation("junit:junit:4.13.2")
    testImplementation("com.github.seeseemelk:MockBukkit-v1.17:1.13.0")
    testImplementation("org.reflections:reflections:0.10.2")

    compileOnly("org.spigotmc:spigot-api:1.18.2-R0.1-SNAPSHOT")
    compileOnly("com.convallyria.languagy:api:3.0.0")
    compileOnly("io.papermc:paperlib:1.0.4") // we include paperlib and relocate elsewhere
    compileOnly("com.github.MilkBowl:VaultAPI:1.7") // vault
    compileOnly("me.clip:placeholderapi:2.10.4") // PAPI
    compileOnly(":AlonsoLevels_v2.0-BETA") // alonsolevels
    compileOnly("com.github.shynixn.headdatabase:hdb-api:1.0") // head database
    compileOnly("com.sk89q.worldguard:worldguard-bukkit:7.0.4-SNAPSHOT") {
        exclude("com.destroystokyo.paper")
        exclude("org.spigotmc")
    }
    compileOnly("com.sk89q.worldedit:worldedit-bukkit:7.2.0-SNAPSHOT") {
        exclude("com.google")
        exclude("org.bukkit")
        exclude("org.spigotmc")
    }
    compileOnly("io.lumine:Mythic-Dist:5.0.3-SNAPSHOT") {
        exclude("org.apache.commons")
    }
}

java {
    withSourcesJar()
    withJavadocJar()
}

publishing {
    repositories {
        // See Gradle docs for how to provide credentials to PasswordCredentials
        // https://docs.gradle.org/current/samples/sample_publishing_credentials.html
        maven {
            name = "convallyriaSnapshots"
            url = uri("https://repo.convallyria.com/snapshots/")
            credentials(PasswordCredentials::class)
        }
        maven {
            name = "convallyriaReleases"
            url = uri("https://repo.convallyria.com/releases/")
            credentials(PasswordCredentials::class)
        }
    }
}

configure<PublishingExtension> {
    publications.create<MavenPublication>("maven") {
        from(components["java"])
    }
}