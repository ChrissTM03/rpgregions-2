package net.islandearth.rpgregions.rewards;

import net.islandearth.rpgregions.api.IRPGRegionsAPI;
import net.islandearth.rpgregions.gui.GuiEditable;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;

public class ConsoleCommandReward extends DiscoveryReward {

	@GuiEditable(value = "Command", icon = Material.STICK)
	private final String command;

	public ConsoleCommandReward(IRPGRegionsAPI api) {
		super(api);
		this.command = "say example";
	}

	public ConsoleCommandReward(IRPGRegionsAPI api, String command) {
		super(api);
		this.command = command;
	}

	@Override
	public void award(Player player) {
		Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), command.replace("%player%", player.getName()));
	}

	@Override
	public String getName() {
		return "Console Command";
	}
}