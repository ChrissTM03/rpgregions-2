package net.islandearth.rpgregions.rewards;

import me.blackvein.quests.Quest;
import me.blackvein.quests.Quester;
import me.blackvein.quests.Quests;
import net.islandearth.rpgregions.api.IRPGRegionsAPI;
import net.islandearth.rpgregions.gui.GuiEditable;
import net.islandearth.rpgregions.thread.Blocking;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.concurrent.ExecutionException;

public class QuestReward extends DiscoveryReward implements Blocking {

    @GuiEditable("Quest id")
    private final String questId;

    public QuestReward(IRPGRegionsAPI api) {
        super(api);
        this.questId = "test";
    }

    @Override
    public void award(Player player) {
        Quests quests = JavaPlugin.getPlugin(Quests.class);
        try { // Have to perform blocking operation.
            Quester quester = quests.getStorage().loadQuesterData(player.getUniqueId()).get();
            Quest quest = quests.getQuestById(questId);
            if (quest == null) return;
            quest.completeQuest(quester);
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getName() {
        return "Quest";
    }

    public String getPluginRequirement() {
        return "Quests";
    }
}
