package net.islandearth.rpgregions.rewards;

import net.islandearth.rpgregions.api.IRPGRegionsAPI;
import net.islandearth.rpgregions.gui.GuiEditable;
import org.bukkit.Material;
import org.bukkit.entity.Player;

public class PlayerCommandReward extends DiscoveryReward {

	@GuiEditable(value = "Command", icon = Material.STICK)
	private final String command;

	public PlayerCommandReward(IRPGRegionsAPI api) {
		super(api);
		this.command = "say example";
	}

	public PlayerCommandReward(IRPGRegionsAPI api, String command) {
		super(api);
		this.command = command;
	}

	@Override
	public void award(Player player) {
		player.performCommand(command.replace("%player%", player.getName()));
	}

	@Override
	public String getName() {
		return "Player Command";
	}
}