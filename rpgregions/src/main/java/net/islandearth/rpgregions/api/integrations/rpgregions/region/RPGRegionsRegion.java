package net.islandearth.rpgregions.api.integrations.rpgregions.region;

import com.google.common.collect.ImmutableList;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public abstract class RPGRegionsRegion {

    private final String name;
    private final List<Location> points;
    private int priority;

    public RPGRegionsRegion(final String name) {
        this.name = name;
        this.points = new ArrayList<>();
        this.priority = 1;
    }

    public String getName() {
        return name;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public ImmutableList<Location> getPoints() {
        return ImmutableList.copyOf(points);
    }

    public void addPoint(final Location location) {
        this.points.add(location);
    }

    public void removePoint(final Location location) {
        this.points.remove(location);
    }

    public boolean isWithinBounds(final Player player) {
        return this.isWithinBounds(player.getLocation());
    }

    public abstract boolean isWithinBounds(final Location location);
}
