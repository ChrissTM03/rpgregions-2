package net.islandearth.rpgregions.api.integrations.rpgregions.region;

import org.bukkit.Location;
import org.bukkit.util.Vector;

public class CuboidRegion extends RPGRegionsRegion {

    public CuboidRegion(final String name) {
        super(name);
    }

    @Override
    public boolean isWithinBounds(Location location) {
        if (getPoints().size() < 2) return false;
        Location first = getPoints().get(0);
        Location second = getPoints().get(1);
        final double x1 = first.getX();
        final double x2 = second.getX();
        final double y1 = first.getY();
        final double y2 = second.getY();
        final double z1 = first.getZ();
        final double z2 = second.getZ();
        Vector min = new Vector(Math.min(x1, x2), Math.min(y1, y2), Math.min(z1, z2));
        Vector max = new Vector(Math.max(x1, x2), Math.max(y1, y2), Math.max(z1, z2));
        return location.toVector().isInAABB(min, max);
    }
}
