package net.islandearth.rpgregions.api.integrations.rpgregions.region;

import org.bukkit.Location;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class PolyRegion extends RPGRegionsRegion {

    public PolyRegion(final String name) {
        super(name);
    }

    @Override
    public boolean isWithinBounds(Location location) {
        if (getPoints().size() < 2) return false;
        List<Integer> xPoints = new ArrayList<>();
        List<Integer> yPoints = new ArrayList<>();
        getPoints().forEach(point -> {
            xPoints.add(point.getBlockX());
            yPoints.add(point.getBlockZ());
        });
        Polygon polygon = new Polygon(xPoints.stream().mapToInt(Integer::intValue).toArray(), yPoints.stream().mapToInt(Integer::intValue).toArray(), xPoints.size());
        return polygon.contains(location.getX(), location.getZ());
    }
}
