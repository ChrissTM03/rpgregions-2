package net.islandearth.rpgregions.api.integrations.hooks;

import me.clip.placeholderapi.expansion.PlaceholderExpansion;
import net.islandearth.rpgregions.RPGRegions;
import net.islandearth.rpgregions.managers.data.account.RPGRegionsAccount;
import net.islandearth.rpgregions.thread.Blocking;
import net.islandearth.rpgregions.translation.Translations;
import org.bukkit.entity.Player;

import java.util.concurrent.ExecutionException;

public class PlaceholderRegionHook extends PlaceholderExpansion implements Blocking {

    private final RPGRegions plugin;

    public PlaceholderRegionHook(RPGRegions plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean persist() {
        return true;
    }

    @Override
    public boolean canRegister() {
        return true;
    }

    @Override
    public String getAuthor() {
        return plugin.getDescription().getAuthors().toString();
    }

    @Override
    public String getIdentifier() {
        return "rpgregions";
    }

    @Override
    public String getVersion() {
        return plugin.getDescription().getVersion();
    }

    @Override
    public String onPlaceholderRequest(Player player, String identifier) {
        if (player == null) return "";

        if (identifier.startsWith("discovered_region_")) {
            // We have to do a blocking operation :(
            try {
                RPGRegionsAccount account = plugin.getManagers().getStorageManager().getAccount(player.getUniqueId()).get();
                String region = identifier.replace("discovered_region_", "");
                boolean discovered = account.getDiscoveredRegions().containsKey(region);
                return String.valueOf(discovered);
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
            return "";
        }

        switch (identifier.toLowerCase()) {
            case "region":
                if (plugin.getManagers().getIntegrationManager().getPrioritisedRegion(player.getLocation()).isPresent())
                    return plugin.getManagers().getIntegrationManager().getPrioritisedRegion(player.getLocation()).get().getCustomName();
                else
                    return Translations.UNKNOWN_REGION.get(player).get(0);
            case "discovered_count":
                // We have to do a blocking operation :(
                try {
                    RPGRegionsAccount account = plugin.getManagers().getStorageManager().getAccount(player.getUniqueId()).get();
                    return String.valueOf(account.getDiscoveredRegions().size());
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }
            case "discovered_percentage":
                // We have to do a blocking operation :(
                try {
                    RPGRegionsAccount account = plugin.getManagers().getStorageManager().getAccount(player.getUniqueId()).get();
                    int percent = (account.getDiscoveredRegions().size() / plugin.getManagers().getRegionsCache().getConfiguredRegions().size()) * 100;
                    return String.valueOf(percent);
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }
            case "region_total":
                return String.valueOf(plugin.getManagers().getRegionsCache().getConfiguredRegions().size());
            default:
                return null;
        }
    }
}
