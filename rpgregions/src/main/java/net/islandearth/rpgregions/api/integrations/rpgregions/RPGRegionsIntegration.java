package net.islandearth.rpgregions.api.integrations.rpgregions;

import co.aikar.commands.InvalidCommandArgument;
import com.google.gson.Gson;
import net.islandearth.rpgregions.RPGRegions;
import net.islandearth.rpgregions.api.IRPGRegionsAPI;
import net.islandearth.rpgregions.api.events.RegionsEnterEvent;
import net.islandearth.rpgregions.api.integrations.IntegrationManager;
import net.islandearth.rpgregions.api.integrations.rpgregions.region.RPGRegionsRegion;
import net.islandearth.rpgregions.commands.RPGRegionsIntegrationCommand;
import net.islandearth.rpgregions.managers.data.region.ConfiguredRegion;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerMoveEvent;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

public class RPGRegionsIntegration implements IntegrationManager {

    private final IRPGRegionsAPI plugin;
    private final Map<String, RPGRegionsRegion> regions;

    public RPGRegionsIntegration(IRPGRegionsAPI plugin) {
        this.plugin = plugin;
        this.regions = new HashMap<>();
        // Register command completions and command, it's here because we only want it to enable if enabled.
        if (plugin instanceof RPGRegions rpgRegions) {
            rpgRegions.getCommandManager().getCommandContexts().registerContext(RPGRegionsRegion.class, context -> {
                String id = context.popFirstArg();
                if (rpgRegions.getManagers().getIntegrationManager() instanceof RPGRegionsIntegration rpgRegionsIntegration) {
                    Optional<RPGRegionsRegion> region = rpgRegionsIntegration.getRegion(id);
                    if (region.isPresent()) {
                        return region.get();
                    }
                }
                throw new InvalidCommandArgument("Could not find a region with that id.");
            });
            rpgRegions.getCommandManager().registerCommand(new RPGRegionsIntegrationCommand(rpgRegions));
        }
        plugin.getLogger().info("RPGRegions region integration has been enabled.");
    }

    /*
     * API METHODS
     */

    public Set<RPGRegionsRegion> getRegions(final Location location) {
        List<RPGRegionsRegion> foundRegions = new ArrayList<>();
        for (RPGRegionsRegion region : regions.values()) {
            if (region.isWithinBounds(location)) foundRegions.add(region);
        }
        return Set.copyOf(foundRegions);
    }

    public Optional<RPGRegionsRegion> getRegion(final String name) {
        return Optional.ofNullable(regions.get(name));
    }

    public void addRegion(final RPGRegionsRegion region) {
        regions.put(region.getName(), region);
    }

    public void removeRegion(final RPGRegionsRegion region) {
        this.removeRegion(region.getName());
    }

    public void removeRegion(final String name) {
        regions.remove(name);
    }

    /*
     * OVERRIDES
     */

    @Override
    public boolean isInRegion(Location location) {
        for (RPGRegionsRegion region : regions.values()) {
            if (region.isWithinBounds(location)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void handleMove(PlayerMoveEvent pme) {
        if (pme.getTo() == null) return;
        plugin.debug("Handling movement");
        Player player = pme.getPlayer();
        int oldX = pme.getFrom().getBlockX();
        int oldY = pme.getFrom().getBlockY();
        int oldZ = pme.getFrom().getBlockZ();
        int x = pme.getTo().getBlockX();
        int y = pme.getTo().getBlockY();
        int z = pme.getTo().getBlockZ();
        Set<RPGRegionsRegion> oldRegions = this.getRegions(new Location(player.getWorld(), oldX, oldY, oldZ));
        Set<RPGRegionsRegion> regions = this.getRegions(new Location(player.getWorld(), x, y, z));

        Optional<ConfiguredRegion> configuredRegion = getPrioritisedRegion(pme.getTo());
        configuredRegion.ifPresent(prioritisedRegion -> {
            plugin.debug("Priority region found");
            plugin.debug("Old: " + oldRegions);
            plugin.debug("New: " + regions);
            List<String> stringRegions = new ArrayList<>();
            regions.forEach(region -> {
                if (!prioritisedRegion.getId().equals(region.getName())
                        && checkRequirements(pme, region.getName())) stringRegions.add(region.getName());
            });

            if (checkRequirements(pme, prioritisedRegion.getId())) {
                plugin.debug("Requirements passed, calling");
                stringRegions.add(0, prioritisedRegion.getId());
                Bukkit.getPluginManager().callEvent(new RegionsEnterEvent(player, stringRegions, !oldRegions.equals(regions)));
            }
        });
    }

    @Override
    public Optional<ConfiguredRegion> getPrioritisedRegion(Location location) {
        RPGRegionsRegion highest = null;
        for (String key : regions.keySet()) {
            final RPGRegionsRegion region = regions.get(key);
            if (region.isWithinBounds(location) && (highest == null || region.getPriority() > highest.getPriority())) {
                highest = region;
            }
        }
        return highest == null ? Optional.empty() : plugin.getManagers().getRegionsCache().getConfiguredRegion(highest.getName());
    }

    @Override
    public boolean exists(World location, String region) {
        return regions.containsKey(region);
    }

    @Override
    public Set<String> getAllRegionNames(World world) {
        return regions.keySet();
    }

    @Override
    public @NotNull List<Location> getBoundingBoxPoints(Location regionLocation, @Nullable String regionId) {
        for (RPGRegionsRegion region : regions.values()) {
            if (region.isWithinBounds(regionLocation)) {
                return region.getPoints();
            }
        }
        return new ArrayList<>();
    }

    public void onEnable() {
        File regionsDir = new File(plugin.getDataFolder() + File.separator + "integration" + File.separator + "regions");
        if (!regionsDir.exists()) regionsDir.mkdirs();
        for (File file : regionsDir.listFiles()) {
            try (Reader reader = new FileReader(file)) {
                Gson gson = plugin.getGson();
                RPGRegionsRegion region = gson.fromJson(reader, RPGRegionsRegion.class);
                if (region == null) continue;
                regions.put(region.getName(), region);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void onDisable() {
        save();
    }

    public void save() {
        File regionsDir = new File(plugin.getDataFolder() + File.separator + "integration" + File.separator + "regions");
        if (!regionsDir.exists()) regionsDir.mkdirs();

        regions.forEach((name, region) -> {
            File file = new File(regionsDir + File.separator + name + ".json");
            try (Writer writer = new FileWriter(file)) {
                Gson gson = plugin.getGson();
                gson.toJson(region, RPGRegionsRegion.class, writer);
                writer.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
}
