package net.islandearth.rpgregions.commands;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Subcommand;
import net.islandearth.rpgregions.RPGRegions;
import net.islandearth.rpgregions.api.events.RegionDiscoverEvent;
import net.islandearth.rpgregions.gui.DiscoveryGUI;
import net.islandearth.rpgregions.managers.data.region.ConfiguredRegion;
import net.islandearth.rpgregions.managers.data.region.WorldDiscovery;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@CommandAlias("discoveries|discovery")
public class DiscoveriesCommand extends BaseCommand {

    private final RPGRegions plugin;

    public DiscoveriesCommand(final RPGRegions plugin) {
        this.plugin = plugin;
    }

    @Default
    @CommandPermission("rpgregions.list")
    public void onDefault(Player player) {
        new DiscoveryGUI(plugin, player).open();
    }

    @Subcommand("discover")
    @CommandPermission("rpgregions.discover")
    @CommandCompletion("@regions @offline-players")
    public void onDiscover(CommandSender sender, ConfiguredRegion configuredRegion, OfflinePlayer target) {
        plugin.getManagers().getStorageManager().getAccount(target.getUniqueId()).thenAccept(account -> {
            LocalDateTime date = LocalDateTime.now();
            DateTimeFormatter format = DateTimeFormatter.ofPattern(plugin.getConfig().getString("settings.server.discoveries.date.format"));

            String formattedDate = date.format(format);
            final WorldDiscovery worldDiscovery = new WorldDiscovery(formattedDate, configuredRegion.getId());
            account.addDiscovery(worldDiscovery);
            if (target.getPlayer() != null) {
                Player player = target.getPlayer();
                player.sendMessage(ChatColor.GREEN + "An administrator added a discovery to your account.");
                Bukkit.getPluginManager().callEvent(new RegionDiscoverEvent(player, configuredRegion.getId(), worldDiscovery));
            }
            plugin.getManagers().getStorageManager().removeCachedAccount(target.getUniqueId());

            sender.sendMessage(ChatColor.GREEN + "The player " + target.getName() + " has had the discovery added.");
        });
    }
}
