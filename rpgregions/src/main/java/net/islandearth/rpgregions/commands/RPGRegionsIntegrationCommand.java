package net.islandearth.rpgregions.commands;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.CommandHelp;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.HelpCommand;
import co.aikar.commands.annotation.Subcommand;
import net.islandearth.rpgregions.RPGRegions;
import net.islandearth.rpgregions.api.integrations.rpgregions.RPGRegionsIntegration;
import net.islandearth.rpgregions.api.integrations.rpgregions.region.CuboidRegion;
import net.islandearth.rpgregions.api.integrations.rpgregions.region.PolyRegion;
import net.islandearth.rpgregions.api.integrations.rpgregions.region.RPGRegionsRegion;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Locale;
import java.util.Optional;

@CommandAlias("rpgri|rpgrintegration")
@CommandPermission("rpgregions.integration")
public class RPGRegionsIntegrationCommand extends BaseCommand {

    private final RPGRegions plugin;

    public RPGRegionsIntegrationCommand(final RPGRegions plugin) {
        this.plugin = plugin;
    }

    @Default
    public void onDefault(CommandSender sender) {
        sender.sendMessage(ChatColor.GREEN + "RPGRegions region integration is enabled. Type /rpgri help for help.");
    }

    @HelpCommand
    @Subcommand("help")
    public void onHelp(final CommandHelp commandHelp) {
        commandHelp.showHelp();
    }

    @Subcommand("list")
    public void onList(final CommandSender sender) {
        RPGRegionsIntegration integration = (RPGRegionsIntegration) plugin.getManagers().getIntegrationManager();
        integration.getAllRegionNames(null).forEach(name -> sender.sendMessage(ChatColor.GREEN + "- " + name));
    }

    @Subcommand("info")
    @CommandCompletion("@regions")
    public void onInfo(final CommandSender sender, final String name) {
        RPGRegionsIntegration integration = (RPGRegionsIntegration) plugin.getManagers().getIntegrationManager();
        Optional<RPGRegionsRegion> region = integration.getRegion(name);
        if (region.isPresent()) {
            sender.sendMessage(ChatColor.GREEN + "Name: " + ChatColor.WHITE + region.get().getName());
            sender.sendMessage(ChatColor.GREEN + "Priority: " + ChatColor.WHITE + region.get().getPriority());
            sender.sendMessage(ChatColor.GREEN + "Points for " + region.get().getName() + ":");
            region.get().getPoints().forEach(point -> sender.sendMessage(" " + point.toString()));
            return;
        }
        sender.sendMessage(ChatColor.RED + "Region " + name + " does not exist!");
    }

    @Subcommand("save")
    public void onSave(final CommandSender sender) {
        RPGRegionsIntegration integration = (RPGRegionsIntegration) plugin.getManagers().getIntegrationManager();
        integration.save();
        sender.sendMessage(ChatColor.GREEN + "Regions saved.");
    }

    @Subcommand("create")
    @CommandCompletion("@regions @region-types")
    public void onCreate(final CommandSender sender, final String name, final String regionType) {
        RPGRegionsRegion region = switch (regionType.toLowerCase(Locale.ENGLISH)) {
            case "cuboid" -> new CuboidRegion(name);
            case "poly" -> new PolyRegion(name);
            default -> null;
        };

        if (region == null) {
            sender.sendMessage(ChatColor.RED + "A region of the type " + regionType + " cannot be found!");
            return;
        }

        RPGRegionsIntegration integration = (RPGRegionsIntegration) plugin.getManagers().getIntegrationManager();
        integration.addRegion(region);
        sender.sendMessage(ChatColor.GREEN + "Created region " + name + ".");
    }

    @Subcommand("delete")
    @CommandCompletion("@regions")
    public void onDelete(final CommandSender sender, final RPGRegionsRegion region) {
        RPGRegionsIntegration integration = (RPGRegionsIntegration) plugin.getManagers().getIntegrationManager();
        integration.removeRegion(region);
        sender.sendMessage(ChatColor.GREEN + "Region " + region.getName() + " has been removed.");
    }

    @Subcommand("addpos")
    @CommandCompletion("@regions")
    public void onAddPos(final Player player, final RPGRegionsRegion region) {
        region.addPoint(player.getLocation());
        player.sendMessage(ChatColor.GREEN + "Added point to " + region.getName() + ".");
    }

    @Subcommand("setpriority")
    @CommandCompletion("@regions @range:20")
    public void onSetPriority(final Player player, final RPGRegionsRegion region, final int priority) {
        region.setPriority(priority);
        player.sendMessage(ChatColor.GREEN + "Set priority of " + region.getName() + " to " + priority + ".");
    }
}
