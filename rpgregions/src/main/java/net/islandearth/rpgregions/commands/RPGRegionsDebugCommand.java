package net.islandearth.rpgregions.commands;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Subcommand;
import co.aikar.idb.DB;
import co.aikar.idb.Database;
import net.islandearth.rpgregions.RPGRegions;
import net.islandearth.rpgregions.effects.RegionEffect;
import net.islandearth.rpgregions.managers.data.IStorageManager;
import net.islandearth.rpgregions.requirements.RegionRequirement;
import net.islandearth.rpgregions.rewards.DiscoveryReward;
import net.islandearth.rpgregions.thread.Blocking;
import org.apache.commons.lang.StringUtils;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import java.sql.Connection;
import java.sql.SQLException;

@CommandAlias("rpgregionsdebug|rpgrd")
@CommandPermission("rpgregions.debug")
public class RPGRegionsDebugCommand extends BaseCommand {

    private final RPGRegions plugin;

    public RPGRegionsDebugCommand(final RPGRegions plugin) {
        this.plugin = plugin;
    }

    @Default
    public void onDefault(CommandSender sender) throws SQLException {
        IStorageManager storageManager = plugin.getManagers().getStorageManager();
        sender.sendMessage(ChatColor.GOLD + "Database status:");
        sender.sendMessage(ChatColor.GRAY + "Storage implementation " + storageManager.getClass().getSimpleName() + ".");
        sender.sendMessage(ChatColor.GRAY + "" + storageManager.getCachedAccounts().size()
                + " cached players.");
        sender.sendMessage(ChatColor.GRAY + "Performance (" + storageManager.getTimingsAverage() + " avg. ms)" +
                " (last 3 retrievals): " + StringUtils.join(storageManager.getTimings(), "ms, ") + "ms");
        Database database = DB.getGlobalDatabase();
        if (database != null) {
            Connection connection = database.getConnection();
            sender.sendMessage(ChatColor.GRAY + "Product: " + connection.getMetaData().getDatabaseProductName());
            sender.sendMessage(ChatColor.GRAY + "Version: " + connection.getMetaData().getDatabaseProductVersion());
            sender.sendMessage(ChatColor.GRAY + "Driver: " + connection.getMetaData().getDriverName());
            sender.sendMessage(ChatColor.GRAY + "Version: " + connection.getMetaData().getDriverVersion());
        }

        plugin.getManagers().getRegionsCache().getConfiguredRegions().forEach((name, configuredRegion) -> {
            for (DiscoveryReward reward : configuredRegion.getRewards()) {
                if (reward instanceof Blocking) {
                    sender.sendMessage(ChatColor.RED + "Region " + name + " has blocking class " + reward + ".");
                }
            }
            for (RegionRequirement requirement : configuredRegion.getRequirements()) {
                if (requirement instanceof Blocking) {
                    sender.sendMessage(ChatColor.RED + "Region " + name + " has blocking class " + requirement + ".");
                }
            }
            for (RegionEffect effect : configuredRegion.getEffects()) {
                if (effect instanceof Blocking) {
                    sender.sendMessage(ChatColor.RED + "Region " + name + " has blocking class " + effect + ".");
                }
            }
        });
    }

    @Subcommand("cache")
    public void onCache(CommandSender sender) {
        IStorageManager storageManager = plugin.getManagers().getStorageManager();
        sender.sendMessage(ChatColor.GOLD + "Database cache:");
        storageManager.getCachedAccounts().forEach((uuid, account) -> {
            sender.sendMessage(ChatColor.GRAY + " - " + uuid.toString());
        });
    }
}
