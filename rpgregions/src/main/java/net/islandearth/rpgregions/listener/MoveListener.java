package net.islandearth.rpgregions.listener;

import net.islandearth.rpgregions.RPGRegions;
import net.islandearth.rpgregions.managers.data.region.ConfiguredRegion;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerTeleportEvent;

public record MoveListener(RPGRegions plugin) implements Listener {

    @EventHandler
    public void onMove(PlayerMoveEvent event) {
        final Player player = event.getPlayer();
        plugin.getManagers().getIntegrationManager().handleMove(event);

        if (!plugin.getManagers().getIntegrationManager().isInRegion(event.getTo()) && plugin.getManagers().getIntegrationManager().isInRegion(event.getFrom())) {
            for (ConfiguredRegion configuredRegion : plugin.getManagers().getRegionsCache().getConfiguredRegions().values()) {
                configuredRegion.getEffects().forEach(effect -> effect.uneffect(player));
            }
        }
    }

    @EventHandler
    public void onTeleport(PlayerTeleportEvent event) {
        final Player player = event.getPlayer();
        plugin.getManagers().getIntegrationManager().handleMove(event);
        if (!plugin.getManagers().getIntegrationManager().isInRegion(event.getTo()) && plugin.getManagers().getIntegrationManager().isInRegion(event.getFrom())) {
            for (ConfiguredRegion configuredRegion : plugin.getManagers().getRegionsCache().getConfiguredRegions().values()) {
                configuredRegion.getEffects().forEach(effect -> effect.uneffect(player));
            }
        }
    }
}