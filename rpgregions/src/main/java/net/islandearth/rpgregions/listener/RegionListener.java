package net.islandearth.rpgregions.listener;

import net.islandearth.rpgregions.RPGRegions;
import net.islandearth.rpgregions.api.events.RegionDiscoverEvent;
import net.islandearth.rpgregions.api.events.RegionsEnterEvent;
import net.islandearth.rpgregions.managers.data.IRPGRegionsCache;
import net.islandearth.rpgregions.managers.data.region.ConfiguredRegion;
import net.islandearth.rpgregions.managers.data.region.Discovery;
import net.islandearth.rpgregions.managers.data.region.WorldDiscovery;
import net.islandearth.rpgregions.translation.Translations;
import net.islandearth.rpgregions.utils.RegenUtils;
import net.islandearth.rpgregions.utils.TitleAnimator;
import net.islandearth.rpgregions.utils.XSound;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class RegionListener implements Listener {

    private final RPGRegions plugin;
    private final List<UUID> titleCooldown;
    private final DateTimeFormatter format;

    public RegionListener(RPGRegions plugin) {
        this.plugin = plugin;
        this.titleCooldown = new ArrayList<>();
        this.format = DateTimeFormatter.ofPattern(plugin.getConfig().getString("settings.server.discoveries.date.format"));
    }

    /**
     * Handles region discoveries on enter.
     */
    @EventHandler
    public void onEnter(RegionsEnterEvent event) {
        Player player = event.getPlayer();
        plugin.getManagers().getStorageManager().getAccount(player.getUniqueId()).thenAccept(account -> {
            for (String region : event.getRegions()) {
                plugin.debug("Checking region: " + region);
                plugin.getManagers().getRegionsCache().getConfiguredRegion(region).ifPresent(configuredRegion -> {
                    boolean has = false;
                    boolean prioritised = event.getPriority().equals(region);
                    for (Discovery discoveredRegion : account.getDiscoveredRegions().values()) {
                        if (discoveredRegion.getRegion().equals(region)) {
                            has = true;
                            break;
                        }
                    }
                    plugin.debug("Has the player discovered this region? " + has);
                    plugin.debug("Is this the prioritised region? " + prioritised);
                    
                    Bukkit.getScheduler().runTask(plugin, () -> this.checkEffects(configuredRegion, player));

                    if (configuredRegion.alwaysShowTitles() && event.hasChanged() && has && prioritised) {
                        this.sendTitles(player, configuredRegion, false);
                    }

                    if (configuredRegion.showActionbar() && event.hasChanged() && prioritised) {
                        player.spigot().sendMessage(ChatMessageType.ACTION_BAR, new TextComponent(Translations.REGION_ENTER_ACTIONBAR.get(player, configuredRegion.getCustomName()).get(0)));
                    }

                    if (!has && configuredRegion.isDiscoverable() && prioritised) {
                        plugin.debug("Discovering region.");
                        LocalDateTime date = LocalDateTime.now();
                        String formattedDate = date.format(format);
                        Discovery discovery = new WorldDiscovery(formattedDate, region);
                        account.addDiscovery(discovery);
                        Bukkit.getPluginManager().callEvent(new RegionDiscoverEvent(player, region, discovery));
                    } else if (prioritised && configuredRegion.isDiscoverable() && has) {
                        if (configuredRegion.getRewards() != null) configuredRegion.getRewards().forEach(reward -> {
                            if (reward.isAlwaysAward()) {
                                reward.award(player);
                            }
                        });
                    }
                });
            }
        });
    }

    @EventHandler
    public void onDiscover(RegionDiscoverEvent rde) {
        Player player = rde.getPlayer();
        String region = rde.getRegion();
        IRPGRegionsCache regionsCache = plugin.getManagers().getRegionsCache();
        regionsCache.getConfiguredRegion(region).ifPresent(configuredRegion -> {
            this.sendTitles(player, configuredRegion, true);

            if (configuredRegion.getSound() == null) {
                player.playSound(
                        player.getLocation(),
                        XSound.valueOf(plugin.getConfig().getString("settings.server.discoveries.discovered.sound.name")).parseSound(),
                        1,
                        plugin.getConfig().getInt("settings.server.discoveries.discovered.sound.pitch")
                );
            } else {
                player.playSound(
                        player.getLocation(),
                        configuredRegion.getSound(),
                        1,
                        plugin.getConfig().getInt("settings.server.discoveries.discovered.sound.pitch")
                );
            }

            if (configuredRegion.getRewards() != null) configuredRegion.getRewards().forEach(reward -> reward.award(player));

            if (configuredRegion.getRegenerate() != null
                    && configuredRegion.getRegenerate().isOnDiscover()) {
                RegenUtils.regenerate(configuredRegion);
            }
        });
    }

    private void sendTitles(Player player, ConfiguredRegion configuredRegion, boolean discovered) {
        if (titleCooldown.contains(player.getUniqueId())) {
            plugin.debug("Player is on title cooldown");
            return;
        }
        
        plugin.debug("Added to title cooldown");
        titleCooldown.add(player.getUniqueId());
        Bukkit.getScheduler().runTaskLater(plugin, () -> {
            plugin.debug("Removed from title cooldown");
            titleCooldown.remove(player.getUniqueId());
        }, plugin.getConfig().getInt("settings.server.discoveries.discovered.title.cooldown"));
        if (!discovered) {
            List<String> discoveredTitle = configuredRegion.getDiscoveredTitle(player);
            List<String> discoveredSubtitle = configuredRegion.getDiscoveredSubtitle(player);
            plugin.debug("Region is not discovered, sending discovery titles! " + discoveredTitle + ":" + discoveredSubtitle);
            new TitleAnimator(player,
                    plugin,
                    discoveredTitle,
                    discoveredSubtitle,
                    plugin.getConfig().getInt("settings.server.discoveries.discovered.title.animation_speed"));
            return;
        }

        List<String> title = configuredRegion.getTitle(player);
        List<String> subtitle = configuredRegion.getSubtitle(player);
        plugin.debug("Region is discovered, sending discovery titles! " + title + ":" + subtitle);
        new TitleAnimator(player,
                plugin,
                title,
                subtitle,
                plugin.getConfig().getInt("settings.server.discoveries.discovered.title.animation_speed"));
    }

    private void checkEffects(ConfiguredRegion configuredRegion, Player player) {
        if (configuredRegion.getEffects() != null) {
            plugin.debug("Checking effects");
            configuredRegion.getEffects().forEach(regionEffect -> {
                boolean canEffect = true;
                if (regionEffect.isWearingRequired()) {
                    for (ItemStack itemStack : player.getInventory().getArmorContents()) {
                        if (regionEffect.shouldIgnore(itemStack)) {
                            canEffect = false;
                            break;
                        }
                    }
                    plugin.debug("Wearing required, canEffect? " + canEffect);
                    if (canEffect) regionEffect.effect(player);
                    return;
                }

                for (ItemStack itemStack : player.getInventory()) {
                    if (regionEffect.shouldIgnore(itemStack)) {
                        canEffect = false;
                        break;
                    }
                }

                plugin.debug("Wearing not required, canEffect? " + canEffect);
                if (canEffect) regionEffect.effect(player);
            });
        }
    }
}
