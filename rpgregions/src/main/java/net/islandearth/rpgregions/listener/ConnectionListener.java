package net.islandearth.rpgregions.listener;

import net.islandearth.rpgregions.RPGRegions;
import net.islandearth.rpgregions.managers.data.account.RPGRegionsAccount;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.UUID;

public record ConnectionListener(RPGRegions plugin) implements Listener {

    @EventHandler
    public void onJoin(AsyncPlayerPreLoginEvent event) {
        UUID uuid = event.getUniqueId();
        try {
            RPGRegionsAccount account = plugin.getManagers().getStorageManager().getAccount(uuid).get();
            if (account != null) return;
        } catch (Exception e) {
            e.printStackTrace();
        }

        event.disallow(AsyncPlayerPreLoginEvent.Result.KICK_OTHER, ChatColor.RED + "RPGRegions account could not be loaded.");
    }

    @EventHandler
    public void onLeave(PlayerQuitEvent pqe) {
        Player player = pqe.getPlayer();
        if (plugin.getManagers().getStorageManager().getCachedAccounts().containsKey(player.getUniqueId()))
            plugin.getManagers().getStorageManager().removeCachedAccount(player.getUniqueId());
    }
}
