package net.islandearth.rpgregions.managers.data.yml;

import net.islandearth.rpgregions.RPGRegions;
import net.islandearth.rpgregions.managers.data.IStorageManager;
import net.islandearth.rpgregions.managers.data.account.RPGRegionsAccount;
import net.islandearth.rpgregions.managers.data.region.Discovery;
import net.islandearth.rpgregions.managers.data.region.WorldDiscovery;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class YamlStorage implements IStorageManager {

    private final ConcurrentMap<UUID, RPGRegionsAccount> cachedAccounts = new ConcurrentHashMap<>();

    private final RPGRegions plugin;

    public YamlStorage(RPGRegions plugin) {
        this.plugin = plugin;
        File dataFile = new File(plugin.getDataFolder() + "/accounts/");
        dataFile.mkdirs();
    }

    @Override
    public CompletableFuture<RPGRegionsAccount> getAccount(UUID uuid) {
        // Add a check to ensure accounts aren't taking a long time
        long startTime = System.currentTimeMillis();
        CompletableFuture<RPGRegionsAccount> future = new CompletableFuture<>();
        future.thenAccept(account -> {
            long endTime = System.currentTimeMillis();
            long totalTime = endTime - startTime;
            timing(totalTime);
        });

        if (cachedAccounts.containsKey(uuid)) {
            future.complete(cachedAccounts.get(uuid));
        } else {
            File file = new File(plugin.getDataFolder() + "/accounts/" + uuid.toString() + ".yml");
            FileConfiguration config = YamlConfiguration.loadConfiguration(file);
            Map<String, Discovery> regions = new HashMap<>();
            for (String results : config.getStringList("Discoveries")) {
                String[] data = results.split(";");
                String time = data[0];
                String region = data[1];
                regions.put(region, new WorldDiscovery(time, region));
            }

            RPGRegionsAccount account = new RPGRegionsAccount(uuid, regions);
            cachedAccounts.putIfAbsent(uuid, account);
            future.complete(account);
        }
        return future;
    }

    @Override
    public ConcurrentMap<UUID, RPGRegionsAccount> getCachedAccounts() {
        return cachedAccounts;
    }

    @Override
    public void clearDiscoveries(UUID uuid) {
        getAccount(uuid).thenAccept(account -> {
            account.getDiscoveredRegions().clear();
        }).exceptionally(t -> {
            t.printStackTrace();
            return null;
        });

        File file = new File(plugin.getDataFolder() + "/accounts/" + uuid.toString() + ".yml");
        FileConfiguration config = YamlConfiguration.loadConfiguration(file);
        config.set("Discoveries", null);
        try {
            config.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void clearDiscovery(UUID uuid, String regionId) {
        getAccount(uuid).thenAccept(account -> {
            account.getDiscoveredRegions().remove(regionId);
        }).exceptionally(t -> {
            t.printStackTrace();
            return null;
        });

        File file = new File(plugin.getDataFolder() + "/accounts/" + uuid.toString() + ".yml");
        FileConfiguration config = YamlConfiguration.loadConfiguration(file);
        Map<String, Discovery> regions = new HashMap<>();
        for (String results : config.getStringList("Discoveries")) {
            String[] data = results.split(";");
            String time = data[0];
            String region = data[1];
            regions.put(region, new WorldDiscovery(time, region));
        }

        regions.remove(regionId);

        List<String> newData = config.getStringList("Discoveries");
        newData.clear();
        for (Discovery region : regions.values()) {
            newData.add(region.getDate() + ";" + region.getRegion());
        }

        config.set("Discoveries", newData);
        try {
            config.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteAccount(UUID uuid) {
        this.clearDiscoveries(uuid);
        File file = new File(plugin.getDataFolder() + "/accounts/" + uuid.toString() + ".yml");
        try {
            Files.deleteIfExists(file.toPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
        cachedAccounts.remove(uuid);
    }

    @Override
    public CompletableFuture<Void> removeCachedAccount(UUID uuid) {
        RPGRegionsAccount account = cachedAccounts.get(uuid);
        File file = new File(plugin.getDataFolder() + "/accounts/" + uuid.toString() + ".yml");
        FileConfiguration config = YamlConfiguration.loadConfiguration(file);

        List<String> newData = new ArrayList<>();
        for (Discovery region : account.getDiscoveredRegions().values()) {
            newData.add(region.getDate() + ";" + region.getRegion());
        }

        config.set("Discoveries", newData);
        try {
            config.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        cachedAccounts.remove(uuid);
        return CompletableFuture.completedFuture(null);
    }
}
