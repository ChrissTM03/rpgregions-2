package net.islandearth.rpgregions.managers.data;

import co.aikar.idb.DB;
import co.aikar.idb.DbRow;
import net.islandearth.rpgregions.RPGRegions;
import net.islandearth.rpgregions.managers.data.account.RPGRegionsAccount;
import net.islandearth.rpgregions.managers.data.region.Discovery;
import net.islandearth.rpgregions.managers.data.region.WorldDiscovery;
import org.intellij.lang.annotations.Language;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public abstract class SQLCommonStorage implements IStorageManager {

    protected static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS rpgregions_discoveries (uuid varchar(32) NOT NULL, region varchar(32) NOT NULL, time varchar(64) NOT NULL, PRIMARY KEY(uuid, region))";
    protected static final String SELECT_REGION = "SELECT * FROM rpgregions_discoveries WHERE uuid = ?";
    protected static final String INSERT_DISCOVERY = "INSERT INTO rpgregions_discoveries (uuid, region, time) VALUES (?, ?, ?)";
    protected static final String DELETE_DISCOVERIES = "DELETE * FROM rpgregions_discoveries WHERE uuid = ?";
    protected static final String DELETE_DISCOVERY = "DELETE * FROM rpgregions_discoveries WHERE uuid = ? AND region = ?";

    private final ConcurrentMap<UUID, RPGRegionsAccount> cachedAccounts = new ConcurrentHashMap<>();

    private final RPGRegions plugin;

    public SQLCommonStorage(RPGRegions plugin) {
        this.plugin = plugin;
    }

    @Override
    public CompletableFuture<RPGRegionsAccount> getAccount(UUID uuid) {
        // Add a check to ensure accounts aren't taking a long time
        long startTime = System.currentTimeMillis();
        CompletableFuture<RPGRegionsAccount> future = new CompletableFuture<>();
        future.thenAccept(account -> {
            long endTime = System.currentTimeMillis();
            long totalTime = endTime - startTime;
            timing(totalTime);
        });

        if (cachedAccounts.containsKey(uuid)) {
            future.complete(cachedAccounts.get(uuid));
        } else {
            DB.getResultsAsync(SELECT_REGION, getDatabaseUuid(uuid)).thenAccept(results -> {
                Map<String, Discovery> regions = new HashMap<>();
                for (DbRow row : results) {
                    String region = row.getString("region");
                    regions.put(region, new WorldDiscovery(row.getString("time"), region));
                }

                RPGRegionsAccount account = new RPGRegionsAccount(uuid, regions);
                cachedAccounts.put(uuid, account);
                future.complete(account);
            }).exceptionally(t -> {
                t.printStackTrace();
                return null;
            });
        }
        return future;
    }

    @Override
    public ConcurrentMap<UUID, RPGRegionsAccount> getCachedAccounts() {
        return cachedAccounts;
    }

    @Override
    public void clearDiscoveries(UUID uuid) {
        getAccount(uuid).thenAccept(account -> account.getDiscoveredRegions().clear()).exceptionally(t -> {
            t.printStackTrace();
            return null;
        });

        DB.executeUpdateAsync(DELETE_DISCOVERIES, getDatabaseUuid(uuid));
    }

    @Override
    public void clearDiscovery(UUID uuid, String regionId) {
        getAccount(uuid).thenAccept(account -> account.getDiscoveredRegions().remove(regionId)).exceptionally(t -> {
            t.printStackTrace();
            return null;
        });

        DB.executeUpdateAsync(DELETE_DISCOVERY, getDatabaseUuid(uuid), regionId);
    }

    @Override
    public void deleteAccount(UUID uuid) {
        this.clearDiscoveries(uuid);
        cachedAccounts.remove(uuid);
    }

    @Override
    public CompletableFuture<Void> removeCachedAccount(UUID uuid) {
        RPGRegionsAccount account = cachedAccounts.get(uuid);
        return DB.getResultsAsync(SELECT_REGION, getDatabaseUuid(uuid)).thenAccept(results -> {
            List<String> current = new ArrayList<>();
            for (DbRow row : results) {
                current.add(row.getString("region"));
            }

            for (Discovery region : account.getDiscoveredRegions().values()) {
                if (!current.contains(region.getRegion())) {
                    executeInsert(INSERT_DISCOVERY, getDatabaseUuid(uuid), region.getRegion(), region.getDate());
                }
            }
            cachedAccounts.remove(uuid);
        }).exceptionally(t -> {
            t.printStackTrace();
            return null;
        });
    }

    protected void executeInsert(@Language("SQL") String query, Object... params) {
        try {
            DB.executeInsert(query, params);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
