package net.islandearth.rpgregions.gui;

import com.github.stefvanschie.inventoryframework.gui.GuiItem;
import com.github.stefvanschie.inventoryframework.gui.type.ChestGui;
import com.github.stefvanschie.inventoryframework.pane.OutlinePane;
import com.github.stefvanschie.inventoryframework.pane.PaginatedPane;
import com.github.stefvanschie.inventoryframework.pane.Pane;
import com.github.stefvanschie.inventoryframework.pane.StaticPane;
import com.github.stefvanschie.inventoryframework.pane.util.Mask;
import io.papermc.lib.PaperLib;
import me.arcaniax.hdb.api.HeadDatabaseAPI;
import net.islandearth.rpgregions.RPGRegions;
import net.islandearth.rpgregions.command.IconCommand;
import net.islandearth.rpgregions.managers.data.account.RPGRegionsAccount;
import net.islandearth.rpgregions.managers.data.region.ConfiguredRegion;
import net.islandearth.rpgregions.requirements.DependencyRequirement;
import net.islandearth.rpgregions.requirements.RegionRequirement;
import net.islandearth.rpgregions.translation.Translations;
import net.islandearth.rpgregions.utils.ItemStackBuilder;
import net.islandearth.rpgregions.utils.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class DiscoveryGUI extends RPGRegionsGUI {

    private final RPGRegions plugin;
    private final Player player;
    private ChestGui gui;

    public DiscoveryGUI(RPGRegions plugin, Player player) {
        super(plugin, player);
        this.plugin = plugin;
        this.player = player;
    }

    @Override
    public void render() {
        this.gui = new ChestGui(plugin.getConfig().getInt("settings.server.gui.general.rows"), Translations.REGIONS.get(player).get(0));
        gui.setOnGlobalClick(click -> click.setCancelled(true));
        PaginatedPane pane = new PaginatedPane(paneX, paneY, paneL, paneH, Pane.Priority.HIGHEST);
        OutlinePane oPane = new OutlinePane(oPaneX, oPaneY, oPaneL, oPaneH);
        OutlinePane innerPane = new OutlinePane(iPaneX, iPaneY, iPaneL, iPaneH);
        StaticPane back = new StaticPane(backX, backY, backL, backH, Pane.Priority.HIGHEST);
        StaticPane forward = new StaticPane(forwardX, forwardY, forwardL, forwardH, Pane.Priority.HIGHEST);
        StaticPane exit = new StaticPane(exitX, exitY, exitL, exitH, Pane.Priority.HIGH);

        // Inner pane
        if (plugin.getConfig().getBoolean("settings.server.gui.innerPane.show")) {
            innerPane.setRepeat(true);
            List<String> mask = plugin.getConfig().getStringList("settings.server.gui.innerPane.mask");
            innerPane.applyMask(new Mask(mask.toArray(new String[]{})));
            innerPane.setOnClick(inventoryClickEvent -> inventoryClickEvent.setCancelled(true));

            innerPane.addItem(new GuiItem(new ItemStackBuilder(Material.valueOf(
                    plugin.getConfig().getString("settings.server.gui.innerPane.innerPane")))
                    .withName(" ")
                    .addFlags(ItemFlag.HIDE_ATTRIBUTES)
                    .build()));

            gui.addPane(innerPane);
        }

        // Outline pane
        if (plugin.getConfig().getBoolean("settings.server.gui.outlinePane.show")) {
            oPane.setRepeat(true);
            List<String> mask = plugin.getConfig().getStringList("settings.server.gui.outlinePane.mask");
            oPane.applyMask(new Mask(mask.toArray(new String[]{})));
            oPane.setOnClick(inventoryClickEvent -> inventoryClickEvent.setCancelled(true));

            oPane.addItem(new GuiItem(new ItemStackBuilder(Material.valueOf(
                    plugin.getConfig().getString("settings.server.gui.outlinePane.outlinePane")))
                    .withName(" ")
                    .addFlags(ItemFlag.HIDE_ATTRIBUTES)
                    .build()));

            gui.addPane(oPane);
        }

        // Back item
        String bm = plugin.getConfig().getString("settings.server.gui.back.back");
        ItemStack backItem = bm.startsWith("hdb-") && plugin.hasHeadDatabase()
                ?
                new ItemStackBuilder(new HeadDatabaseAPI().getItemHead(bm.replace("hdb-", "")))
                        .withName(Translations.PREVIOUS_PAGE.get(player).get(0))
                        .withLore(Translations.PREVIOUS_PAGE_LORE.get(player))
                        .addFlags(ItemFlag.HIDE_ATTRIBUTES)
                        .build()
                :
                new ItemStackBuilder(Material.valueOf(
                        plugin.getConfig().getString("settings.server.gui.back.back")))
                        .withName(Translations.PREVIOUS_PAGE.get(player).get(0))
                        .withLore(Translations.PREVIOUS_PAGE_LORE.get(player))
                        .addFlags(ItemFlag.HIDE_ATTRIBUTES)
                        .build();

        back.addItem(new GuiItem(backItem, event -> {
            event.setCancelled(true);
            if (pane.getPages() == 0 || pane.getPages() == 1) return;

            pane.setPage(pane.getPage() - 1);

            forward.setVisible(true);
            gui.update();
        }), 0, 0);

        // Forward item
        String fm = plugin.getConfig().getString("settings.server.gui.forward.forward");
        ItemStack forwardItem = fm.startsWith("hdb-") && plugin.hasHeadDatabase()
                ?
                new ItemStackBuilder(new HeadDatabaseAPI().getItemHead(fm.replace("hdb-", "")))
                        .withName(Translations.NEXT_PAGE.get(player).get(0))
                        .withLore(Translations.NEXT_PAGE_LORE.get(player))
                        .addFlags(ItemFlag.HIDE_ATTRIBUTES)
                        .build()
                :
                new ItemStackBuilder(Material.valueOf(
                        plugin.getConfig().getString("settings.server.gui.forward.forward")))
                        .withName(Translations.NEXT_PAGE.get(player).get(0))
                        .withLore(Translations.NEXT_PAGE_LORE.get(player))
                        .addFlags(ItemFlag.HIDE_ATTRIBUTES)
                        .build();

        forward.addItem(new GuiItem(forwardItem, event -> {
            event.setCancelled(true);
            if (pane.getPages() == 0 || pane.getPages() == 1) return;

            pane.setPage(pane.getPage() + 1);

            back.setVisible(true);
            gui.update();
        }), 0, 0);

        // Exit item
        if (plugin.getConfig().getBoolean("settings.server.gui.exit.show")) {
            String em = plugin.getConfig().getString("settings.server.gui.exit.exit");
            ItemStack item = em.startsWith("hdb-") && plugin.hasHeadDatabase()
                    ?
                    new ItemStackBuilder(new HeadDatabaseAPI().getItemHead(em.replace("hdb-", "")))
                            .withName(Translations.EXIT.get(player).get(0))
                            .withLore(Translations.EXIT_LORE.get(player))
                            .addFlags(ItemFlag.HIDE_ATTRIBUTES)
                            .build()
                    :
                    new ItemStackBuilder(Material.valueOf(
                            plugin.getConfig().getString("settings.server.gui.exit.exit")))
                            .withName(Translations.EXIT.get(player).get(0))
                            .withLore(Translations.EXIT_LORE.get(player))
                            .addFlags(ItemFlag.HIDE_ATTRIBUTES)
                            .build();
            exit.addItem(new GuiItem(item, event -> {
                event.setCancelled(true);
                gui.update();
                player.closeInventory();
                String command = plugin.getConfig().getString("settings.server.gui.exit.command");
                if (!command.isEmpty()) player.performCommand(command
                        .replace("%player%", player.getName()));
            }), 0, 0);

            gui.addPane(exit);
        }

        gui.addPane(back);
        gui.addPane(forward);

        plugin.getManagers().getStorageManager().getAccount(player.getUniqueId()).thenAccept(account -> {
            List<GuiItem> items = new ArrayList<>();
            for (ConfiguredRegion configuredRegion : plugin.getManagers().getRegionsCache().getConfiguredRegions().values()) {
                boolean hasDiscovered = account.getDiscoveredRegions().containsKey(configuredRegion.getId());
                if ((!hasDiscovered && !player.hasPermission("rpgregions.show"))
                        || configuredRegion.isHidden()) continue;

                ChatColor colour = hasDiscovered
                        ? ChatColor.valueOf(plugin.getConfig().getString("settings.server.discoveries.discovered.name-colour"))
                        : ChatColor.valueOf(plugin.getConfig().getString("settings.server.discoveries.undiscovered.name-colour"));
                List<String> lore = account.getDiscoveredRegions().containsKey(configuredRegion.getId())
                        ? Translations.DISCOVERED_ON.get(player,
                        account.getDiscoveredRegions().get(configuredRegion.getId()).getDate())
                        : null;
                String lore2 = configuredRegion.isShowCoords()
                        && player.hasPermission("rpgregions.showloc")
                        ? ChatColor.GRAY + "" + configuredRegion.getLocation().getBlockX() + ", " + configuredRegion.getLocation().getBlockZ()
                        : null;
                List<String> translatedHint = new ArrayList<>();
                if (configuredRegion.getHints() != null) {
                    configuredRegion.getHints().forEach(hint -> translatedHint.add(StringUtils.colour(hint)));
                }

                List<String> hint = configuredRegion.isShowHint()
                        && player.hasPermission("rpgregions.showhint." + configuredRegion.getId()) || player.hasPermission("rpgregions.showhint.*")
                        && !hasDiscovered
                        ? translatedHint
                        : null;
                List<String> teleport = configuredRegion.isTeleportable()
                        && player.hasPermission("rpgregions.teleport")
                        && hasDiscovered
                        ? Translations.TELEPORT.get(player)
                        : null;
                List<String> requirementStrings = new ArrayList<>();
                boolean requirements = true;
                if (!player.hasPermission("rpgregions.bypassentry") && configuredRegion.getRequirements() != null) {
                    for (RegionRequirement requirement : configuredRegion.getRequirements()) {
                        boolean meets = requirement.meetsRequirements(player);
                        if (requirement instanceof DependencyRequirement dependencyRequirement) {
                            List<String> discoveries = new ArrayList<>(account.getDiscoveredRegions().keySet());
                            meets = dependencyRequirement.meetsRequirements(discoveries);
                        }
                        if (!meets) {
                            requirements = false;
                            requirementStrings.add(ChatColor.RED + "\u2718 " + requirement.getText(player));
                        } else {
                            requirementStrings.add(ChatColor.GREEN + "\u2714 " + requirement.getText(player));
                        }
                    }
                }

                List<String> translatedDiscoveredLore = new ArrayList<>();
                if (configuredRegion.getDiscoveredLore() != null) {
                    configuredRegion.getDiscoveredLore().forEach(discoveredLore -> translatedDiscoveredLore.add(StringUtils.colour(discoveredLore)));
                }
                ItemStack item = hasDiscovered
                        ?
                        new ItemStackBuilder(configuredRegion.getIcon())
                                .withLore(translatedDiscoveredLore)
                                .build()
                        :
                        new ItemStackBuilder(configuredRegion.getUndiscoveredIcon())
                                .build();

                boolean finalRequirements = requirements;
                items.add(new GuiItem(new ItemStackBuilder(item)
                        .withName(colour + configuredRegion.getCustomName())
                        .withLore(lore)
                        .withLore(lore2)
                        .withLore(hint)
                        .withLore(" ")
                        .withLore(requirementStrings)
                        .withLore(teleport)
                        .addFlags(ItemFlag.HIDE_ATTRIBUTES)
                        .build(),
                        event -> {
                            event.setCancelled(true);
                            if (configuredRegion.isTeleportable()
                                    && player.hasPermission("rpgregions.teleport")
                                    && hasDiscovered) {
                                if (!account.getCooldowns().contains(RPGRegionsAccount.AccountCooldown.TELEPORT)) {
                                    if (configuredRegion.getWorld() == null || !finalRequirements) {
                                        Translations.CANNOT_TELEPORT.send(player);
                                    } else {
                                        if (configuredRegion.getLocation() != null) PaperLib.teleportAsync(player, configuredRegion.getLocation());
                                        else player.sendMessage(ChatColor.RED + "Unable to find teleport location.");
                                        if (configuredRegion.getTeleportCooldown() != 0) {
                                            account.getCooldowns().add(RPGRegionsAccount.AccountCooldown.TELEPORT);
                                            Bukkit.getScheduler().runTaskLater(plugin, () -> account.getCooldowns().remove(RPGRegionsAccount.AccountCooldown.TELEPORT), configuredRegion.getTeleportCooldown());
                                        }
                                    }
                                } else {
                                    Translations.COOLDOWN.send(player);
                                }
                            }

                            if (!configuredRegion.getIconCommand().isEmpty()) {
                                configuredRegion.getIconCommand().forEach(iconCommand -> {
                                    if (iconCommand.getClickType() != IconCommand.CommandClickType.DISCOVERED && hasDiscovered
                                            || iconCommand.getClickType() != IconCommand.CommandClickType.UNDISCOVERED && !hasDiscovered) {
                                        return;
                                    }

                                    if (account.getCooldowns().contains(RPGRegionsAccount.AccountCooldown.ICON_COMMAND)) {
                                        Translations.COOLDOWN.send(player);
                                        return;
                                    }

                                    player.performCommand(iconCommand.getCommand()
                                            .replace("%region%", configuredRegion.getId())
                                            .replace("%player%", player.getName()));

                                    if (iconCommand.getCooldown() != 0) {
                                        account.getCooldowns().add(RPGRegionsAccount.AccountCooldown.ICON_COMMAND);
                                        Bukkit.getScheduler().runTaskLater(plugin, () -> account.getCooldowns().remove(RPGRegionsAccount.AccountCooldown.ICON_COMMAND), iconCommand.getCooldown());
                                    }
                                });
                            }
                        }));
            }
            pane.populateWithGuiItems(items);
            gui.addPane(pane);
        }).exceptionally(error -> {
            plugin.getLogger().warning("There was an error whilst listing regions");
            error.printStackTrace();
            return null;
        });
    }

    @Override
    public ChestGui getGui() {
        return gui;
    }
}
