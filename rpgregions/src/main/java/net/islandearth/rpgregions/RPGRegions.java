package net.islandearth.rpgregions;

import co.aikar.commands.InvalidCommandArgument;
import co.aikar.commands.PaperCommandManager;
import co.aikar.idb.DB;
import com.convallyria.languagy.api.language.Translator;
import com.google.common.collect.ImmutableList;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import net.islandearth.rpgregions.api.IRPGRegionsAPI;
import net.islandearth.rpgregions.api.RPGRegionsAPI;
import net.islandearth.rpgregions.api.integrations.rpgregions.RPGRegionsIntegration;
import net.islandearth.rpgregions.api.integrations.rpgregions.region.RPGRegionsRegion;
import net.islandearth.rpgregions.commands.DiscoveriesCommand;
import net.islandearth.rpgregions.commands.RPGRegionsCommand;
import net.islandearth.rpgregions.commands.RPGRegionsDebugCommand;
import net.islandearth.rpgregions.effects.FogEffect;
import net.islandearth.rpgregions.effects.PotionRegionEffect;
import net.islandearth.rpgregions.effects.RegionEffect;
import net.islandearth.rpgregions.effects.RegionEffectRegistry;
import net.islandearth.rpgregions.effects.VanishEffect;
import net.islandearth.rpgregions.exception.CouldNotStartException;
import net.islandearth.rpgregions.gson.AbstractAdapter;
import net.islandearth.rpgregions.gson.ItemStackAdapter;
import net.islandearth.rpgregions.gson.LocationAdapter;
import net.islandearth.rpgregions.gson.PotionEffectAdapter;
import net.islandearth.rpgregions.listener.ConnectionListener;
import net.islandearth.rpgregions.listener.MoveListener;
import net.islandearth.rpgregions.listener.RegionListener;
import net.islandearth.rpgregions.listener.ServerReloadListener;
import net.islandearth.rpgregions.managers.RPGRegionsManagers;
import net.islandearth.rpgregions.managers.data.region.ConfiguredRegion;
import net.islandearth.rpgregions.managers.registry.RPGRegionsRegistry;
import net.islandearth.rpgregions.requirements.AlonsoLevelRequirement;
import net.islandearth.rpgregions.requirements.DependencyRequirement;
import net.islandearth.rpgregions.requirements.ItemRequirement;
import net.islandearth.rpgregions.requirements.LevelRequirement;
import net.islandearth.rpgregions.requirements.MoneyRequirement;
import net.islandearth.rpgregions.requirements.PermissionRequirement;
import net.islandearth.rpgregions.requirements.PlaceholderRequirement;
import net.islandearth.rpgregions.requirements.QuestRequirement;
import net.islandearth.rpgregions.requirements.RegionRequirement;
import net.islandearth.rpgregions.requirements.RegionRequirementRegistry;
import net.islandearth.rpgregions.rewards.AlonsoLevelReward;
import net.islandearth.rpgregions.rewards.ConsoleCommandReward;
import net.islandearth.rpgregions.rewards.DiscoveryReward;
import net.islandearth.rpgregions.rewards.ExperienceReward;
import net.islandearth.rpgregions.rewards.ItemReward;
import net.islandearth.rpgregions.rewards.MessageReward;
import net.islandearth.rpgregions.rewards.MoneyReward;
import net.islandearth.rpgregions.rewards.PlayerCommandReward;
import net.islandearth.rpgregions.rewards.QuestReward;
import net.islandearth.rpgregions.rewards.RegionDiscoverReward;
import net.islandearth.rpgregions.rewards.RegionRewardRegistry;
import net.islandearth.rpgregions.rewards.TeleportReward;
import net.islandearth.rpgregions.tasks.DynmapTask;
import net.islandearth.rpgregions.translation.Translations;
import org.bstats.bukkit.Metrics;
import org.bstats.charts.SimplePie;
import org.bstats.charts.SingleLineChart;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;

public final class RPGRegions extends JavaPlugin implements IRPGRegionsAPI {
	
	private RPGRegionsManagers managers;
	private PaperCommandManager commandManager;

	public PaperCommandManager getCommandManager() {
		return commandManager;
	}

	@Override
	public Translator getTranslator() {
		return translator;
	}

	private Translator translator;

	@Override
	public void onEnable() {
		RPGRegionsAPI.setAPI(this);
		this.createConfig();
		this.generateLang();
		this.commandManager = new PaperCommandManager(this);
		try {
			this.managers = new RPGRegionsManagers(this);
		} catch (ReflectiveOperationException | IOException | CouldNotStartException e) {
			getLogger().log(Level.SEVERE, "Error starting managers, please report this!", e);
			Bukkit.getPluginManager().disablePlugin(this);
			return;
		}
		this.registerRewards();
		this.registerRequirements();
		this.registerEffects();
		this.registerListeners();
		this.registerCommands();
		this.translator = Translator.of(this);
		this.registerTasks();
		this.registerMetrics();

		// Tell integration to enable
		if (managers.getIntegrationManager() instanceof RPGRegionsIntegration rpgRegionsIntegration) {
			rpgRegionsIntegration.onEnable();
		}
	}

	@Override
	public void onDisable() {
		translator.close();

		if (managers == null || managers.getRegionsCache() == null || managers.getStorageManager() == null) {
			getLogger().warning("Unable to save data as managers were null");
		} else {
			// Save all player data (quit event not called for shutdown)
			Bukkit.getOnlinePlayers().forEach(player -> {
				if (managers.getStorageManager().getCachedAccounts().containsKey(player.getUniqueId())) {
					try {
						this.getManagers().getStorageManager().removeCachedAccount(player.getUniqueId()).get();
					} catch (InterruptedException | ExecutionException e) {
						Thread.currentThread().interrupt();
					}
				}
			});

			// Tell integration to save
			if (managers.getIntegrationManager() instanceof RPGRegionsIntegration rpgRegionsIntegration) {
				rpgRegionsIntegration.onDisable();
			}

			// Save all region configs
			managers.getRegionsCache().getConfiguredRegions().forEach((id, region) -> region.save(this));
		}

		RPGRegionsAPI.setAPI(null);
		DB.close();
	}

	@Override
	public RPGRegionsManagers getManagers() {
		return managers;
	}

	private void generateLang() {
		Translations.generateLang(this);
	}

	private void createConfig() {
		saveDefaultConfig(); // Moved to config.yml
	}

	private void registerListeners() {
		PluginManager pm = Bukkit.getPluginManager();
		pm.registerEvents(new ServerReloadListener(this), this);
		pm.registerEvents(new ConnectionListener(this), this);
		pm.registerEvents(new RegionListener(this), this);
		pm.registerEvents(new MoveListener(this), this);
	}

	private void registerCommands() {
		final PaperCommandManager manager = commandManager;
		manager.enableUnstableAPI("help");
		manager.getCommandCompletions().registerAsyncCompletion("regions", context -> ImmutableList.copyOf(getManagers().getRegionsCache().getConfiguredRegions().keySet()));
		manager.getCommandCompletions().registerAsyncCompletion("integration-regions", context -> ImmutableList.copyOf(this.getManagers().getIntegrationManager().getAllRegionNames(context.getPlayer().getWorld())));
		manager.getCommandCompletions().registerAsyncCompletion("async", context -> ImmutableList.of("--async"));
		manager.getCommandCompletions().registerAsyncCompletion("region-types", context -> ImmutableList.of("Cuboid", "Poly"));
		manager.getCommandCompletions().registerAsyncCompletion("offline-players", context -> {
			List<String> players = new ArrayList<>();
			if (!getConfig().getBoolean("settings.server.tabcomplete.search-offline-players")) {
				for (Player player : Bukkit.getOnlinePlayers()) {
					players.add(player.getName());
				}
				return ImmutableList.copyOf(players);
			}

			for (OfflinePlayer offlinePlayer : Bukkit.getOfflinePlayers()) {
				players.add(offlinePlayer.getName());
			}
			return ImmutableList.copyOf(players);
		});
		manager.getCommandCompletions().registerAsyncCompletion("schematics", context -> {
			File schematicFolder = new File("plugins/WorldEdit/schematics/");
			List<String> files = new ArrayList<>();
			for (File file : schematicFolder.listFiles()) {
				files.add(file.getName());
			}
			return files;
		});
		manager.getCommandContexts().registerContext(ConfiguredRegion.class, context -> {
			String id = context.popFirstArg();
			for (ConfiguredRegion region : managers.getRegionsCache().getConfiguredRegions().values()) {
				if (region.getId().equals(id)) {
					return region;
				}
			}
			throw new InvalidCommandArgument("Could not find a region with that id.");
		});
		manager.registerCommand(new RPGRegionsCommand(this));
		manager.registerCommand(new DiscoveriesCommand(this));
		manager.registerCommand(new RPGRegionsDebugCommand(this));
	}

	private void registerRewards() {
		RPGRegionsRegistry<DiscoveryReward> registry = (RegionRewardRegistry) managers.getRegistry(RegionRewardRegistry.class);
		if (registry == null) {
			getLogger().warning("Unable to register rewards");
			return;
		}
		registry.register(ConsoleCommandReward.class);
		registry.register(ExperienceReward.class);
		registry.register(ItemReward.class);
		registry.register(MessageReward.class);
		registry.register(MoneyReward.class);
		registry.register(PlayerCommandReward.class);
		registry.register(AlonsoLevelReward.class);
		registry.register(QuestReward.class);
		registry.register(TeleportReward.class);
		registry.register(RegionDiscoverReward.class);
	}

	private void registerRequirements() {
		RPGRegionsRegistry<RegionRequirement> registry = (RegionRequirementRegistry) managers.getRegistry(RegionRequirementRegistry.class);
		if (registry == null) {
			getLogger().warning("Unable to register requirements");
			return;
		}

		registry.register(AlonsoLevelRequirement.class);
		registry.register(ItemRequirement.class);
		registry.register(LevelRequirement.class);
		registry.register(MoneyRequirement.class);
		registry.register(PlaceholderRequirement.class);
		registry.register(DependencyRequirement.class);
		registry.register(QuestRequirement.class);
		registry.register(PermissionRequirement.class);
	}

	private void registerEffects() {
		RPGRegionsRegistry<RegionEffect> registry = (RegionEffectRegistry) managers.getRegistry(RegionEffectRegistry.class);
		if (registry == null) {
			getLogger().warning("Unable to register effects");
			return;
		}
		registry.register(PotionRegionEffect.class);
		registry.register(FogEffect.class);
		registry.register(VanishEffect.class);
	}

	@Override
	public Gson getGson() {
		return new GsonBuilder()
				.registerTypeAdapter(DiscoveryReward.class, new AbstractAdapter<DiscoveryReward>(null))
				.registerTypeAdapter(RegionEffect.class, new AbstractAdapter<RegionEffect>(null))
				.registerTypeAdapter(RegionRequirement.class, new AbstractAdapter<RegionRequirement>(null))
				.registerTypeAdapter(RPGRegionsRegion.class, new AbstractAdapter<RPGRegionsRegion>(null))
				.registerTypeHierarchyAdapter(PotionEffect.class, new PotionEffectAdapter(this))
				.registerTypeHierarchyAdapter(ItemStack.class, new ItemStackAdapter())
				.registerTypeHierarchyAdapter(Location.class, new LocationAdapter())
				.setPrettyPrinting()
				.serializeNulls().create();
	}

	@Override
	public boolean hasHeadDatabase() {
		return Bukkit.getPluginManager().getPlugin("HeadDatabase") != null;
	}

	private void registerTasks() {
		if (Bukkit.getPluginManager().getPlugin("Dynmap") != null
				&& getConfig().getBoolean("settings.external.dynmap")) {
			Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new DynmapTask(this), 0L, 20L);
			getLogger().info("Registered support for Dynmap.");
		}
	}

	private void registerMetrics() {
		Metrics metrics = new Metrics(this, 2066);
		// regions_discovered chart currently causes lag due to bStats not running it async :(
		if (getConfig().getBoolean("settings.metrics.send_custom_info")) {
			/*metrics.addCustomChart(new SingleLineChart("regions_discovered", () -> {
				debug("Submitting custom metrics on thread: " + Thread.currentThread().getName());
				int discoveries = 0;
				for (OfflinePlayer offlinePlayer : Bukkit.getOfflinePlayers()) {
					RPGRegionsAccount account = getManagers().getStorageManager().getAccount(offlinePlayer.getUniqueId()).get();
					discoveries = discoveries + account.getDiscoveredRegions().values().size();
					Player player = Bukkit.getPlayer(offlinePlayer.getUniqueId());
					if (player == null)
						getManagers().getStorageManager().removeCachedAccount(offlinePlayer.getUniqueId()); // Cleanup so we don't use memory
				}
				return discoveries;
			}));*/
		}
		metrics.addCustomChart(new SingleLineChart("regions_configured", () -> getManagers().getRegionsCache().getConfiguredRegions().size()));
		metrics.addCustomChart(new SimplePie("storage_mode", () -> getConfig().getString("settings.storage.mode")));
		metrics.addCustomChart(new SimplePie("integration_type", () -> getConfig().getString("settings.integration.name")));
	}

	@Override
	public boolean debug() {
		return this.getConfig().getBoolean("settings.dev.debug");
	}

	@Override
	public void debug(String debug) {
		if (debug()) this.debug(debug, Level.INFO);
	}

	public void debug(String debug, Level level) {
		if (debug()) this.getLogger().log(level, "[Debug] " + debug);
	}
}
