plugins {
    id("com.github.johnrengelman.shadow") version("7.1.2")
    id("java")
}

java.sourceCompatibility = JavaVersion.VERSION_16
java.targetCompatibility = JavaVersion.VERSION_16

dependencies {
    testImplementation("junit:junit:4.13.2")
    implementation(project(":rpgregions", "shadow"))
    implementation(project(":modern", "shadow"))
    implementation(project(":api", "shadow"))
}

allprojects {
    group = "net.islandearth.rpgregions"
    version = "1.4.3"

    //tasks.withType(JavaCompile).configureEach {
    // ensure that the encoding is set to UTF-8, no matter what the system default is
    // this fixes some edge cases with special characters not displaying correctly
    // see http://yodaconditions.net/blog/fix-for-java-file-encoding-problems-with-gradle.html
    // If Javadoc is generated, this must be specified in that task too.
    //it.options.encoding = "UTF-8"

    // Force warnings
    //options.compilerArgs << '-Xlint:all'
    //options.deprecation = true
    //}

    apply(plugin = "com.github.johnrengelman.shadow")
    apply(plugin = "java")

    repositories {
        mavenCentral()
        mavenLocal()

        maven("https://hub.spigotmc.org/nexus/content/repositories/snapshots/")
        maven("https://repo.codemc.io/repository/maven-snapshots/")
        maven("https://jitpack.io")
        maven("https://repo.convallyria.com/releases")
        maven("https://papermc.io/repo/repository/maven-public/")
        maven("https://oss.sonatype.org/content/groups/public/")
        maven("https://repo.aikar.co/content/groups/aikar/")

        // worldguard
        maven("https://maven.enginehub.org/repo/")

        // PAPI
        maven("https://repo.extendedclip.com/content/repositories/placeholderapi/")

        // MythicMobs
        maven("https://mvn.lumine.io/repository/maven-public/")

        // Dynmap
        maven("https://repo.mikeprimm.com")

        flatDir { dir("../libraries") }
    }

    tasks {
        test {
            useJUnitPlatform()

            testLogging {
                events("passed", "skipped", "failed")
            }
        }

        shadowJar {
            archiveClassifier.set("")
        }

        build {
            dependsOn(shadowJar)
        }

        processResources {
            filesMatching("plugin.yml") {
                expand("version" to project.version)
            }
        }
    }
}